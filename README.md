# pkg-catapult

A wrapper around *sett* for automated creation and transfer of data
packages to landing zones.

## Dependencies

* [sett](https://sett.readthedocs.io/en/latest/index.html)

## Usage

*pkg-catapult* automates and times the processes of *sett* package creation
(encryption + compression) and transfer to a **destination** landing zone.
It also provides a **batch mode** that allows to package and transfer several
batches of files automatically.

To use *pkg-catapult*, a JSON configuration file with one or more
**destinations** connections must be created at the following location:

* `~/.config/pkg-catapult/config.json` on Linux/Mac.
* `C:\Users\<username>\AppData\Roaming\pkg-catapult\config.json` on Windows.

Each destination must contain the following sections:

* `connection`: SFTP connection parameters to the destination server.
* `dtr_id`: BioMedIT Data Transfer Request ID number to use for the transfer.
  Please use a test project or do a local transfer - see the
  [Local testing](#Local testing) section below.
* `default_recipients`: recipient of the data transfer, i.e. the email of the
  person for which the data will be encrypted.

An example of a configuration file with a destination named, **area51** is
shown below, and a [pre-filled config template file is available here](config.json.template):

```json
{
  "destinations": {
    "area51": {
      "connection": {
        "host": "lz.area51.gov",
        "destination_dir": "upload",
        "username": "chucknorris",
        "pkey": "/path/to/pkey"
      },
      "dtr_id": 42,
      "default_recipients": ["Gunnery Sgt. Hartmann"]
    }
  }
}
```

After the configuration file is created, *pkg-catapult* can be invoked by
passing one or more **route** values to it. A **route** is a string that has
one of the following two forms:

* `"destination"`: a string consisting of the name of a **single destination**,
  as defined in the config file. If such a route is given as input, data is
  transferred to the "destination" destination directly.
* `"transit_node->destination"`: a string that consists of **two destination**
  names separated by a `->`. This will upload the data to the "transit_node"
  server (which must also be defined as a destination in the config file), before
  sending the data to the final "destination". The final destination of the
  route must be the landing zone that hosts your project.

The example below shows 3 invocations examples of *pkg-catapult*.
**Important:** note the usage of **quotes around the route strings**, as the
command will fail without them.

```bash
./pkg-catapult.py 'area51'                    # transfer file to area51.
./pkg-catapult.py 'area51->area52'            # transfer file to area52 via area51.
./pkg-catapult.py 'area51->area52' 'area51'   # do both of the above.
```

**Note:** for most use cases (outside of testing), the **route** value is a
single `"destination"`. In this case the quotes around the route string can
be omitted.

### Optional arguments

*pkg-catapult* accepts following optional arguments:

* `-f`/`--files`: pass a specific file or directory to be used as input data
  for the data package created and transferred by *sett*. More than one file
  and/or directory can be passed by repeating the `-f/--files` option several
  times, e.g. `-f input_file_1 -f input_file_2 -f input_dir`.
  If no input file is specified, *pkg-catapult* uses a small one-line file
  as default input.
* `-b`/`--batch`: specify a "batch" file instead of passing individual files.
  This option is mutually exclusive with `-f`/`--files`. See
  "Using pkg-catapult in batch mode" section below.
* `-s`/`--sender`: specify an email/fingerprint value to use as data sender.
  By default, *pkg-catapult* uses a user's default private PGP key as sender.
* `-r`/`--recipient`: specify one or more recipients that will override the
  `default_recipients` given in the config file.
* `-k`/`--keep-pkg`: if this flag is present, the encrypted data package
  created by *sett* is not deleted after the transfer completes. The default
  behavior of *pkg-catapult* is to delete data packages upon exit (successful
  or not). Note the *pkg-catapult* will not delete any original input file.
* `-c`/`--create-only`: if this flag is present, the data package is created by
  *sett*, but transfer is not performed.
* `--tmpdir`/`-t` option allows to manually specify a directory where
  encrypted *sett* packages are written before they get transferred.
  By default, this directory is `tempfile.gettempdir()`. It overwrites
  `output_dir` and `temp_dir` in *sett* configuration.

### Using pkg-catapult in batch mode

To automate the packaging and transfer of several file sets (one ore more
files/directories to be packaged and transferred together), *pkg-catapult*
implements a so-called "batch" mode.

Batch mode is activated by passing a batch file via the `-b`/`--batch` option,
as shown in the example below. Note that in this case, the `-f`/`--files`
option should be omitted.

```bash
./pkg-catapult.py --batch batch_file.txt area51
```

The batch file, in the example above `batch_file.txt`, must be a plain text
file where each line contains a commma-separated list of files/directories to
be packaged and transferred together. A line can also contain a single file or
directory. Comments can be added using the `#` symbol (see example below).

Here is an example of a batch file with 3 jobs, i.e. it will create and
transfer 3 data packages. The first data package will contain only "file_A",
the second will contain files "file_B", "file_C" and "file_D", and the third
will contain "directory_A" (the entire content of the directory is packaged)
and "file_E".

```bash
# Lines starting with hash are considered comments and ignored.
file_A
file_B, file_C, file_D     # Comments can also be added at the end of a line.
directory_A, file_E
```

### Local testing

Running a local test of *pkg-catapult* without uploading data to an actual
landing zone is possible using one of the following methods:

* Run `pkg-catapult` with the `-c`/`--create-only` flag. This will create a
  data package locally, but not transfer it.
* Run `pkg-catapult` with a destination set to a local SFTP server, e.g.
  running in a containerized environment, as will be illustrated here).

To run `pkg-catapult` with local, containerized SFTP server, you should first
create a new `"test"` destination in the config file, as shown below.
Note that you will need to replace Chuck Norris' email with your own, as well
as enter a valid `dtr_id` (use the DTR_ID of one of your projects).

If you have copied the [config template file](config.json.template), a
pre-filled "test" destination is already included.

```json
{
  "destinations": {
    "test": {
      "connection": {
        "host": "localhost:2222",
        "destination_dir": "upload",
        "username": "sftp",
        "pkey": "~/.ssh/pkg-catapult_testkey"
      },
      "dtr_id": DTR_ID_OF_YOUR_TEST_PROJECT,
      "default_recipients": ["chuck.norris@roundhouse.org"]
    }
  }
}
```

With [podman](https://podman.io) or [docker](https://www.docker.com) docker
installed on your local machine, you can then run the following commands (if
you are using `docker` simply replace the `podman` command with `docker` in
the commands):

```bash
# Launch the local SFTP server container:
podman run -d --rm -p 2222:22 --name pkg_catapult_sftp docker.io/biomedit/test-sftp-server:latest
podman cp pkg_catapult_sftp:/home/sftp/.ssh/sftp_ssh_testkey $HOME/.ssh/pkg-catapult_testkey

# Run pkg-catapult:
./pkg-catapult.py -f test_file 'test'

# Shutdown the local SFTP server:
podman stop pkg_catapult_sftp
```
